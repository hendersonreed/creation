#!/bin/bash

# This script creates a governing body for freetown.io. It serves as a charter and constitution
# of the government of freetown.io

# This script will be run once and will initialize the necessary functionality to create, maintain,
# and administer freetown.io through democratic means.


# Citizens form the core of the democratic society of freetown.io. The voting citizenry is made up
# of longtime members of freetown.io and must vote to accept new members into their ranks.

groupadd citizens

# Every user starts as a provisional user. These users are not allowed to vote but may still
# provisionally take part in freetown.io society. 

groupadd prov


# These users have absolute control over the system. They may run any command at any time. 
# Users may not become an executor through any normal process, and these users should only
# use their power when absolutely necessary.

# This power is deemed useful for fixing bugs in the created system, as bugs may render the
# voting citizenry unable to carry out any action. This power may be used for the defense of
# freetown.io, to defend the public and restore order after a breach.

groupadd executors

# And allow executors to execute any command as sudo
echo "executors ALL=(ALL:ALL) ALL" >> /etc/sudoers



# The freetown user owns everything related to the town. This user will run all the automatic
# infrastructure that makes freetown run.

useradd freetown



# The town hall is created as the public forum of freetown.io. This is created as follows:

# /townhall is the root of the town hall. Citizens are allowed to create files and directories
# in this directory, letting citizens actively take part in creating new ways to interact with
# the town. This also makes it easy and encourages citizens to share what they create.

mkdir -p /townhall
chown /townhall freetown:freetown
chmod 775 /townhall
setfacl -m "g:citizens:rwx" /townhall


# /townhall/creation contains the documents used to set up freetown. These are meant mainly
# for documentation and should not be modified.

mkdir -p /townhall/creation
chmod 775 /townhall/creation


# /townhall/acts contains a read-only copy of all scripts that have been voted on and ran. 
# Most of these laws should be self-enforcing as the UNIX environment lets you explicitly
# control what the public is allowed to do. 

mkdir -p /townhall/acts
chmod 775 /townhall/acts

# /townhall/proposed contains scripts that are going to be up for voting at some point
# in the future.

mkdir -p /townhall/proposed
chmod 775 /townhall/proposed

# /townhall/rejected holds all the scripts that have been up for a vote and failed to 
# gain the necessary votes to be run.

mkdir -p /townhall/rejected
chmod 775 /townhall/rejected

# /townhall/laws should contain plain english or simply formatted laws. These are usually 
# not enforceable by the operating system, and are designed to be read and understood by
# the public. Nothing is automaticall created here, but acts can create files in this
# directory.

mkdir -p /townhall/laws
chmod 775 /townhall/laws

